import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { CartService } from '../cart.service';
import { Product } from '../model/product';
import { CartModel } from '../model/cart.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {

  // private _subscriber;
  private cart: CartModel;

  constructor(private _cartService: CartService, private _router: Router) {
    this.cart = this._cartService.getCart();
  }

  updateQuantity(product: Product, newQuantity: number) {
    this._cartService.updateItem({product, quantity: +newQuantity});
  }

  checkout() {
    this._router.navigate(['/checkout']);
  }

  ngOnInit() {
    // this._subscriber = this._cartService.itemAdded$.subscribe(product => this.cart.addProductToCart(product));
  }

  ngOnDestroy() {
    // this._subscriber.unsubscribe();
  }

}
