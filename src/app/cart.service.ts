import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

import { Product } from './model/product';
import { CartModel } from './model/cart.model';
import { CartEntry } from './model/cart-entry';
import { Datastore } from './datastore';

@Injectable()
export class CartService {

  private CART_STORAGE_KEY = 'CART';
  private cart: CartModel = new CartModel();

  private cartCountModifiedSource = new Subject<number>();

  cartCountModified$ = this.cartCountModifiedSource.asObservable();

  constructor(private _datastore: Datastore) {
    this.loadFromLocalStorage();
  }

  private loadFromLocalStorage() {
    let cartEntries = localStorage.getItem(this.CART_STORAGE_KEY);
    if (cartEntries) {
       let parsedCartEntries = JSON.parse(cartEntries);
       parsedCartEntries.forEach(serializableEntry => {
         this._datastore.findRecord(Product, serializableEntry.id)
         .subscribe( (product: Product) => this.addItem(product, serializableEntry.quantity));
       });
    }
  }

  private writeToLocalStorage() {
    let cartEntries: CartEntry[] = this.cart.getCartEntries();
    let serializableEntries = cartEntries.map(entry => { return { id: entry.product.id, quantity: entry.quantity }; });
    localStorage.setItem(this.CART_STORAGE_KEY, JSON.stringify(serializableEntries));
  }

  addItem(product: Product, quantity = 1) {
    this.cart.addEntry({product, quantity});
    this.cartCountModifiedSource.next(this.cart.itemCount());
    this.writeToLocalStorage();
  }

  updateItem(entry: CartEntry) {
    console.log('CartService.updateEntry', entry);
    this.cart.updateEntry(entry);
    this.cartCountModifiedSource.next(this.cart.itemCount());
    this.writeToLocalStorage();
  }

  getCart(): CartModel {
    return this.cart;
  }

  emptyCart() {
    this.cart.empty();
    this.cartCountModifiedSource.next(this.cart.itemCount());
    this.writeToLocalStorage();
  }

}
