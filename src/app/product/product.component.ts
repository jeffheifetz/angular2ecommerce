import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import 'rxjs/add/operator/switchMap';

import { Product } from '../model/product';
import { Datastore } from '../datastore';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: []
})
export class ProductComponent implements OnInit {

  private product: Product;

  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _datastore: Datastore
            ) { }

  ngOnInit() {
    this._route.params
    .switchMap((params: Params) => this._datastore.findRecord(Product, params['id']))
    .subscribe((product: Product) => this.product = product);
  }

}
