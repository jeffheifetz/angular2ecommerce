import { Component } from '@angular/core';

import { CartService } from './cart.service';
import { Datastore } from './datastore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CartService, Datastore]
})
export class AppComponent {
  title = 'app works!';
}
