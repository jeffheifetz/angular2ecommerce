import { Component, OnInit, OnDestroy } from '@angular/core';

import { CartService } from '../cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: []
})
export class HeaderComponent implements OnInit, OnDestroy {

  private _subscriber;
  private cartCount: number = 0;

  constructor(private _cartService: CartService) { }

  ngOnInit() {
    this._subscriber = this._cartService.cartCountModified$.subscribe(count => this.cartCount = count);
  }

  ngOnDestroy() {
    this._subscriber.unsubscribe();
  }

}
