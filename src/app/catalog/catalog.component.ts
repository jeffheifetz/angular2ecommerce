import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../model/product';
import { Datastore } from '../datastore';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css'],
  providers: []
})
export class CatalogComponent implements OnInit, OnDestroy {

  products: Product[] = [];
  productCount: number = 0;
  subscriber;

  constructor(private _datastore: Datastore, private _router: Router, private _cartService: CartService ) { }

  /**
  * update variables used in the template (change detection)
  */
  private updateProducts(products: Product[]) {
    this.productCount = products.length;
    this.products = products.slice(0, 20);
  }

  onSelect(product: Product) {
    this._router.navigate(['/catalog', product.id]);
  }

  addToCart(product: Product) {
    this._cartService.addItem(product, 1);
  }

  ngOnInit() {

    this.subscriber = this._datastore.query(Product, {}).subscribe((products: Product[]) => {
      this.updateProducts(products);
    });
  }

  ngOnDestroy() {
    /**
    * unsubscribe from ProductService event
    */
    this.subscriber.unsubscribe();
  }
}
