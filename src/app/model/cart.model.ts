import { CartEntry } from './cart-entry';
import { Product } from './product';

export class CartModel {

  private cart: { [index: string]: CartEntry } = {};

  constructor() {}

  addEntry(entry: CartEntry) {
    if (this.cart[entry.product.id]) {
      this.cart[entry.product.id].quantity = this.cart[entry.product.id].quantity + entry.quantity;
    } else {
      this.cart[entry.product.id] = entry;
    }

    if (this.cart[entry.product.id].quantity === 0) { delete this.cart[entry.product.id]; }
  }

  updateEntry(entry: CartEntry) {
    this.cart[entry.product.id] = entry;

    if (this.cart[entry.product.id].quantity === 0) { delete this.cart[entry.product.id]; }
  }

  addProductToCart(product: Product) {
    if (this.cart[product.id]) {
      this.cart[product.id].quantity++;
    } else {
      this.cart[product.id] = {product, quantity: 1};
    }
  }

  getCartEntries(): CartEntry[] {
    return Object.values(this.cart);
  }

  itemCount(): number {
    return Object.values(this.cart).map(entry => entry.quantity).reduce((sum, quant) => sum + quant, 0);
  }

  getTotalPrice(): number {
    return Object.values(this.cart).map(entry => entry.product.price).reduce((sum, quant) => sum + quant, 0);
  }

  empty() {
    this.cart = {};
  }
}
