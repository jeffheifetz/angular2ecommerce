import { JsonApiModelConfig, JsonApiModel, Attribute } from 'angular2-jsonapi';

@JsonApiModelConfig({
  type: 'products'
})
export class Product extends JsonApiModel {
  @Attribute()
  id: string;

  @Attribute()
  title: string;

  @Attribute()
  price: number;

  @Attribute()
  image: string;

  @Attribute()
  description: string;

}
