/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { CartModel } from './cart.model';
import { Datastore } from '../datastore';

describe('CartModel', () => {

  it('should create an instance', () => {
    expect(new CartModel()).toBeTruthy();
  });

  it('should convert products to quantities', () => {
    let id = '23123123';
    let title = 'AwesomeProd';
    let price = 41.97;
    let image = 'http://www.awesome.com/awesomeImage.png';
    let description = 'DESCRIBE';
    let datastore = new Datastore();

    let prod = datastore.createRecord(Product, { id, title, price, image, description });

    let cart = new CartModel();

    cart.addProductToCart(prod);
    cart.addProductToCart(prod);

    let entries = cart.getCartEntries();
    expect(entries[0].product).toEqual(prod);
    expect(entries[0].quantity).toEqual(2);

  });
});
