import { EthocaAngularFrontendPage } from './app.po';

describe('ethoca-angular-frontend App', function() {
  let page: EthocaAngularFrontendPage;

  beforeEach(() => {
    page = new EthocaAngularFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
